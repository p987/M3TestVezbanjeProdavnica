package test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.model.Zadatak3;


@Repository
public interface Zadatak3Repository extends JpaRepository<Zadatak3, Long>{
	Zadatak3 findOneById (Long id);
}
