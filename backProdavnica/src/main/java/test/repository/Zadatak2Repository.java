package test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.model.Zadatak2;


@Repository
public interface Zadatak2Repository extends JpaRepository<Zadatak2, Long>{
	Zadatak2 findOneById (Long id);
	
}
