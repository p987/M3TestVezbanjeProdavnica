package test.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.model.Zadatak1;


@Repository
public interface Zadatak1Repository extends JpaRepository<Zadatak1, Long>{
	Zadatak1 findOneById (Long id);
	Page<Zadatak1> findByCenaBetweenAndZadatak2Id (Double minCena, Double maxCena, Long Zadatak2Id, Pageable pageable);
	//Page<Zadatak> findBySprintId (Long SprintId, Pageable pageable);
	Page<Zadatak1> findByCenaBetween(Double minCena, Double maxCena, Pageable pageable);
}
