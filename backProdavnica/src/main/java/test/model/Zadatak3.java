package test.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;



@Entity
//@Table(name= "filmovi")
public class Zadatak3 {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	 
	@Column
    private Integer kolicina;
	
	@OneToOne
    private Zadatak1 zadatak1;

	
	
	public Zadatak3() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Zadatak1 getZadatak1() {
		return zadatak1;
	}

	public void setZadatak1(Zadatak1 zadatak1) {
		this.zadatak1 = zadatak1;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	
}
