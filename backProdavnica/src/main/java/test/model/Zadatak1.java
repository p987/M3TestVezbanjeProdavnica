package test.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;


@Entity
//@Table(name= "filmovi")
public class Zadatak1 {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(nullable=false, unique=true)
    private String naziv;
	
	@Column(nullable=false)
	private Double cena;
	
	@Column
	private Integer stanje;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(referencedColumnName = "id")
	private List<Zadatak2> zadatak2 = new ArrayList<>();
	
	
	public Zadatak1() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Integer getStanje() {
		return stanje;
	}

	public void setStanje(Integer stanje) {
		this.stanje = stanje;
	}

	public void setZadatak2(List<Zadatak2> zadatak2) {
		this.zadatak2 = zadatak2;
	}

	public List<Zadatak2> getZadatak2() {
		return zadatak2;
	}
	
	public Zadatak2 delete(Zadatak2 brisi) {
		if (brisi != null) {
			this.zadatak2.remove(brisi);
		}
		return brisi;
	}
	
	public Zadatak2 add(Zadatak2 dodaj) {
		if (dodaj != null) {
			this.zadatak2.add(dodaj);
		}
		return dodaj;
	}
}
