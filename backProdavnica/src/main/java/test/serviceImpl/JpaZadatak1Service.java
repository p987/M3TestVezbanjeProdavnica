package test.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import test.model.Zadatak1;
import test.repository.Zadatak1Repository;
import test.service.Zadatak1Service;

@Service
public class JpaZadatak1Service implements Zadatak1Service{

	@Autowired
	Zadatak1Repository repository;
	
	@Override
	public List<Zadatak1> getAll() {
		return repository.findAll();
	}

	@Override
	public Zadatak1 getOne(Long id) {
		return repository.findOneById(id);
	}

	@Override
	public Zadatak1 save(Zadatak1 zadatak) {
		return repository.save(zadatak);
	}

	@Override
	public Zadatak1 update(Zadatak1 zadatak) {
		return repository.save(zadatak);
	}

	@Override
	public Zadatak1 delete(Long id) {
		Zadatak1 zadatak;
		zadatak = getOne(id);
		repository.delete(zadatak);
		return zadatak;
	}
	
	@Override
	public Page<Zadatak1> search(Long Zadatak2Id, Double minCena, Double maxCena, int pageNo) {
		if (minCena==null) {
			minCena = 0.0;
		} 
		if (maxCena==null) {
			maxCena = Double.MAX_VALUE;
		}
		if(Zadatak2Id==null) {
			return repository.findByCenaBetween(minCena, maxCena, PageRequest.of(pageNo, 4));
		}
		return repository.findByCenaBetweenAndZadatak2Id(minCena, maxCena, Zadatak2Id, PageRequest.of(pageNo, 4));
	}

}
