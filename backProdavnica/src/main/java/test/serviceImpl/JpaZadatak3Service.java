package test.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.model.Zadatak3;
import test.repository.Zadatak3Repository;
import test.service.Zadatak3Service;

@Service
public class JpaZadatak3Service implements Zadatak3Service {

	@Autowired
	Zadatak3Repository repository;
	
	@Override
	public List<Zadatak3> getAll() {
		return repository.findAll();
	}

	@Override
	public Zadatak3 getOne(Long id) {
		return repository.findOneById(id);
	}

}
