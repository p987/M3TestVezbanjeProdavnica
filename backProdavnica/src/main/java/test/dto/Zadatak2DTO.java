package test.dto;

public class Zadatak2DTO {
	private Long id;
	
	private String naziv;

	
	
	public Zadatak2DTO() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
}
