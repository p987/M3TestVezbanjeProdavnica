package test.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import test.model.Zadatak2;

public class Zadatak1DTO {
	private Long id;
	
	@NotEmpty(message="Polje ime mora biti popunjeno!")
	@NotBlank(message="Niste uneli ime.")
	@Size(max=15, message="Duzina naziva moze imati maksimalno 15 karatkera.")
	private String naziv;
	
	@Positive(message="Cena ne moze biti negativna vrednost!")
	private Double cena;
	
	@Positive(message="Stanje ne moze biti negativna vrednost!")
	private Integer stanje;
	
	private List<Zadatak2> zadatak2 = new ArrayList<>();
	
	@Positive(message="Id kategorije ne moze biti negativan!")
	private Long zadatak2Id;
	
	
	public Zadatak1DTO() {
	}
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public Double getCena() {
		return cena;
	}


	public void setCena(Double cena) {
		this.cena = cena;
	}


	public Integer getStanje() {
		return stanje;
	}


	public void setStanje(Integer stanje) {
		this.stanje = stanje;
	}


	public List<Zadatak2> getZadatak2() {
		return zadatak2;
	}


	public void setZadatak2(List<Zadatak2> zadatak2) {
		this.zadatak2 = zadatak2;
	}


	public Long getZadatak2Id() {
		return zadatak2Id;
	}


	public void setZadatak2Id(Long zadatak2Id) {
		this.zadatak2Id = zadatak2Id;
	}
	
}
