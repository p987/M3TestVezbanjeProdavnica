package test.dto;

import test.model.Zadatak1;

public class Zadatak3DTO {
	private Long id;
	
	private Integer kolicina;

	private Zadatak1 zadatak1;
	
	
	
	public Zadatak3DTO() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public Zadatak1 getZadatak1() {
		return zadatak1;
	}

	public void setZadatak1(Zadatak1 zadatak1) {
		this.zadatak1 = zadatak1;
	}
	
}
