package test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import test.dto.Zadatak3DTO;
import test.model.Zadatak3;
import test.service.Zadatak3Service;
import test.support.DtoToZadatak3;
import test.support.Zadatak3ToDto;



@RestController
@RequestMapping(value="/api/zadatak3", produces=MediaType.APPLICATION_JSON_VALUE)
@Validated
public class Zadatak3Controller {

	@Autowired
	private Zadatak3Service service;
	
	@Autowired
	private DtoToZadatak3 toZadatak3;
	
	@Autowired
	private Zadatak3ToDto toDto;
	
	
	
	@PreAuthorize("hasRole('KORISNIK')")
	@GetMapping
	public ResponseEntity <List<Zadatak3DTO>> getAll(){
		
		List<Zadatak3> zadatak=service.getAll();
		
		return new ResponseEntity<>(toDto.convert(zadatak),HttpStatus.OK);
		
		}
	
	
	
	
	@PreAuthorize("hasRole('KORISNIK')")
    @GetMapping("/{id}")
	public ResponseEntity<Zadatak3DTO> getOne(@PathVariable Long id) {
		
		Zadatak3 zadatak = service.getOne(id);
		
		if (zadatak != null) {
			return new ResponseEntity<>(toDto.convert(zadatak), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
}
