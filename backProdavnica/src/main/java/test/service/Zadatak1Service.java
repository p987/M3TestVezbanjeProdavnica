package test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import test.model.Zadatak1;

public interface Zadatak1Service {
	
	List<Zadatak1> getAll();
	Zadatak1 getOne (Long id);
	Zadatak1 save (Zadatak1 zadatak);
	Zadatak1 update (Zadatak1 zadatak);
	Zadatak1 delete (Long id);
	Page<Zadatak1>search(Long Zadatak2Id,Double minCena,Double maxCena, int pageNo);
}
