package test.service;

import java.util.List;

import test.model.Zadatak3;

public interface Zadatak3Service {
	
	List<Zadatak3> getAll();
	Zadatak3 getOne (Long id);
}
