package test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.dto.Zadatak1DTO;
import test.model.Zadatak1;


@Component
public class Zadatak1ToDto implements Converter<Zadatak1, Zadatak1DTO> {

	@Override
	public Zadatak1DTO convert(Zadatak1 zadatak) {
		Zadatak1DTO dto=new Zadatak1DTO();
		dto.setId(zadatak.getId());
		dto.setNaziv(zadatak.getNaziv());
		dto.setCena(zadatak.getCena());
		dto.setStanje(zadatak.getStanje());
		dto.setZadatak2(zadatak.getZadatak2());
		
		return dto;
	}

	public List<Zadatak1DTO>convert(List<Zadatak1>zadaci){
		List<Zadatak1DTO> zadaciDTO=new ArrayList<>();
		
		for(Zadatak1 zadatak:zadaci) {
			zadaciDTO.add(convert(zadatak));
		}
		return zadaciDTO;
	}
}
