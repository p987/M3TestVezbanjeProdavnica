package test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.dto.Zadatak1DTO;
import test.model.Zadatak1;
import test.service.Zadatak1Service;
import test.service.Zadatak2Service;

@Component
public class DtoToZadatak1 implements Converter<Zadatak1DTO, Zadatak1> {

	@Autowired
	Zadatak1Service zadatak1Service;
	
	@Autowired
	Zadatak2Service zadatak2Service;
	
	
	@Override
	public Zadatak1 convert(Zadatak1DTO dto) {
		Zadatak1 entity;
		if(dto.getId()==null) {
			entity=new Zadatak1();
		}else{
			entity = zadatak1Service.getOne(dto.getId());
		}
		
		if(entity !=null) {
			
			entity.setNaziv(dto.getNaziv());
			entity.setCena(dto.getCena());
			entity.setStanje(dto.getStanje());
			if ( entity.getZadatak2() != null ) {
				entity.getZadatak2().clear();
			}
			entity.getZadatak2().add(zadatak2Service.getOne(dto.getZadatak2Id()));
			
			
		}
		
		return entity;
	}

}
