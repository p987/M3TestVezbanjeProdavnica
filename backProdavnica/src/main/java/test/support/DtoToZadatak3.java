package test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.dto.Zadatak3DTO;
import test.model.Zadatak3;
import test.service.Zadatak3Service;

@Component
public class DtoToZadatak3 implements Converter<Zadatak3DTO, Zadatak3> {

	@Autowired
	Zadatak3Service zadatakService;
	
	@Override
	public Zadatak3 convert(Zadatak3DTO dto) {
		Zadatak3 entity;
		if(dto.getId()==null) {
			entity=new Zadatak3();
		}else{
			entity = zadatakService.getOne(dto.getId());
		}
		
		if(entity !=null) {
			
			entity.setKolicina(dto.getKolicina());
			entity.setZadatak1(dto.getZadatak1());
			
		}
		
		return entity;
	}

}
