package test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.dto.Zadatak3DTO;
import test.model.Zadatak3;

@Component
public class Zadatak3ToDto implements Converter<Zadatak3, Zadatak3DTO>{

	@Override
	public Zadatak3DTO convert(Zadatak3 zadatak) {
		Zadatak3DTO dto = new Zadatak3DTO();
		dto.setId(zadatak.getId());
		dto.setKolicina(zadatak.getKolicina());
		dto.setZadatak1(zadatak.getZadatak1());
		
		return dto;
	}
	
	public List<Zadatak3DTO>convert(List<Zadatak3>zadaci){
		List<Zadatak3DTO> zadaciDTO=new ArrayList<>();
		
		for(Zadatak3 zadatak:zadaci) {
			zadaciDTO.add(convert(zadatak));
		}
		return zadaciDTO;
	}
}
