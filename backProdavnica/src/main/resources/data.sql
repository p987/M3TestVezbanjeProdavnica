INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');

INSERT INTO zadatak1 (id, cena, naziv, stanje) VALUES (1, 300, 'carapa', 12);
INSERT INTO zadatak1 (id, cena, naziv, stanje) VALUES (2, 400, 'majca', 7);
INSERT INTO zadatak1 (id, cena, naziv, stanje) VALUES (3, 500, 'cipela', 8);
INSERT INTO zadatak1 (id, cena, naziv, stanje) VALUES (4, 700, 'kosulja', 4);

INSERT INTO zadatak3 (id, kolicina, zadatak1_id) VALUES (1, 2, 1);
INSERT INTO zadatak3 (id, kolicina, zadatak1_id) VALUES (2, 3, 3);
INSERT INTO zadatak3 (id, kolicina, zadatak1_id) VALUES (3, 1, 1);
INSERT INTO zadatak3 (id, kolicina, zadatak1_id) VALUES (4, 2, 4);
INSERT INTO zadatak3 (id, kolicina, zadatak1_id) VALUES (5, 3, 2);

INSERT INTO zadatak2 (id, naziv, zadatak2_id) VALUES (1, 'niza', 1);
INSERT INTO zadatak2 (id, naziv, zadatak2_id) VALUES (2, 'srednja', 3);
INSERT INTO zadatak2 (id, naziv, zadatak2_id) VALUES (3, 'visoka', 4);
INSERT INTO zadatak2 (id, naziv, zadatak2_id) VALUES (4, 'luksuz', 2);
