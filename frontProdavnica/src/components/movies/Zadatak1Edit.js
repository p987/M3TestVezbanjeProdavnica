import React, { useCallback, useEffect, useState } from 'react';
import TestAxios from '../../apis/TestAxios';
import {Col, Row, Form, Button} from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';

const Zadatak1Edit = (props) => {

    let init = {
        id: "",
        naziv: "",
        cena: "",
        stanje: "",
        zadatak2: "",
        zadatak2Id: ""
    }

    const routeParams = useParams();
    const zadaciId = routeParams.id; 
    const navigate = useNavigate()

    const [editZadatak1, setEditZadatak1] = useState(init);
    const [zadatak2, setZadatak2] = useState([])
    const [zadatak3, setZadatak3] = useState([])

    useEffect(()=>{
        getZadatak1()
        getZadatak2()
        //getZadatak3()
    }, [])

    const getZadatak1 = useCallback(() => {
        TestAxios.get('/zadatak1/' + zadaciId)
            .then(res => {
                // handle success
                console.log(res)
                setEditZadatak1(res.data)
            })
            .catch(error => {
                // handle error
                console.log(error)
                alert('Error occured please try again!')
            });
    },[])

    const getZadatak2 = useCallback(() => {
        TestAxios.get('/zadatak2')
        .then(res => {
            // handle success
            console.log(res)
            setZadatak2(res.data)
        },[])
        .catch(error => {
            // handle error
            console.log(error)
            alert('Error occured please try again!')
        });
        },[])
    
        const getZadatak3 = useCallback(() => {
            TestAxios.get('/zadatak3')
            .then(res => {
                // handle success
                console.log(res)
                setZadatak3(res.data)
            })
            .catch(error => {
                // handle error
                console.log(error)
                alert('Error occured please try again!')
            });
            },[])


            const edit = () => {
                let zadatakDTO = {
                    id: editZadatak1.id,
                    naziv: editZadatak1.naziv,
                    cena: editZadatak1.cena,
                    stanje: editZadatak1.stanje,
                    zadatak2Id: editZadatak1.zadatak2Id
                }
                TestAxios.put("/zadatak1/" + zadaciId, zadatakDTO)
                .then(() => {
                    navigate("/zadatak1");
                })
                .catch(error => {
                    console.log(error)
                    alert('Error while creating zadatak')
                })
            }


    const onChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        editZadatak1[name] = value;
        setEditZadatak1(editZadatak1);
        console.log(editZadatak1);
      };

      const renderZadatak2 = () => {
        return zadatak2.map((zad)=><option key={zad.id} value={zad.id}>{zad.naziv}</option>)
    }

    const renderZadatak3 = () => {
        return zadatak3.map((zad)=><option key={zad.id} value={zad.id}>{zad.naziv}</option>)
    }

return(
        <>
            <Row><h1>Proizvod</h1></Row>
        
            <Row>
            <Col xs="12" sm="10" md="8">
            <Form>
            <Form.Label htmlFor="naziv">Ime</Form.Label>
            <Form.Control
              id="naziv"
              name="naziv"
              type="text"
              defaultValue={editZadatak1.naziv}
              onChange={(e) => onChange(e)}
            />
            
            <Form.Label htmlFor="cena">Cena</Form.Label>
            <Form.Control
              id="cena"
              name="cena"
              type="number"
              defaultValue={editZadatak1.cena}
              onChange={(e) => onChange(e)}
            />
            <Form.Label htmlFor="stanje">Stanje</Form.Label>
            <Form.Control
              id="stanje"
              name="stanje"
              type="number"
              defaultValue={editZadatak1.stanje}
              onChange={(e) => onChange(e)}
            />
            <Form.Label>Kategorija</Form.Label>
            <Form.Control as="select" name="zadatak2Id" onChange={(e) => onChange(e)}>
                <option></option>
                {renderZadatak2()}
            </Form.Control><br />
            <Button style={{ marginTop: "25px" }} onClick={() => edit()}>
              Edit
            </Button>
            </Form>
            </Col>
            </Row>
        </>        
);

}

export default Zadatak1Edit