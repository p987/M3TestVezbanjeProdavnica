import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";


const Zadatak1Row = (props) => {

    var navigate = useNavigate()

    // const getGenresStringFromList = (list) => {
    //     return list.map(element => element.naziv).join(',');
    // }

    const goToEdit = (id) => {
        navigate('/zadatak1/edit/' + id);
    }

    const getGenresStringFromList = (list) => {
        return list.map(element => element.naziv).join(',');
    }

    return (
        <tr>
            {console.log("zadatak2: " + props.zadatak.zadatak2.naziv)}
           <td>{props.zadatak.naziv}</td>
           <td>{props.zadatak.cena}</td>
           <td>{props.zadatak.stanje}</td>
           <td>{getGenresStringFromList(props.zadatak.zadatak2)}</td>
           {window.localStorage['svoja rola']=='ADMIN'?
           <td><Button variant="warning" onClick={() => goToEdit(props.zadatak.id)}>Edit</Button></td> : null}
           {window.localStorage['svoja rola']=='ADMIN'?
           <td><Button variant="danger" onClick={() => props.deleteZadatakCallback(props.zadatak.id)}>Delete</Button></td> : null}
        </tr>
     )
}

export default Zadatak1Row;