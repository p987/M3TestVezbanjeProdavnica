import React, { useCallback, useEffect, useState } from 'react';
import TestAxios from '../../apis/TestAxios';
import { Row, Col, Button, Table, Form } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom';
import Zadatak1Row from "./Zadatak1Row"

const Zadatak1 = (props) => {

    let init = {
        id: "",
        naziv: "",
        cena: "",
        stanje: "",
        zadatak2: "",
        zadatak2Id: ""
    }

    let empty_search = {
        zadatak2Id: "",
        minCena: "",
        maxCena: ""
    }

    const [totalPages, setTotalPages] = useState(0)
    const [pageNo, setPageNo] = useState(0)
    const [showSearch, setShowSearch] = useState(false)
    const [search, setSearch] = useState(empty_search)
    const [zadatak1, setZadatak1] = useState([])
    const [noviZadatak, setNoviZadatak] = useState(init)
    const [zadatak2, setZadatak2] = useState([])
    const [zadatak3, setZadatak3] = useState([])

    useEffect(()=>{
        getZadatak1(0)
        getZadatak2()
        //getZadatak3()
    }, [])

    const getZadatak1 = useCallback((newPageNo) => {
        let config = {
            params: {
                pageNo: newPageNo,
                zadatak2Id: search.ime,
                minCena: search.minCena,
                maxCena: search.maxCena
            }
        }
        TestAxios.get('/zadatak1', config)
            .then(res => {
                // handle success
                console.log(res)
                setPageNo(newPageNo)
                setTotalPages(res.headers['total-pages'])
                setZadatak1(res.data)
                console.log("total pages: " + totalPages)
            })
            .catch(error => {
                // handle error
                console.log(error)
                alert('Error occured please try again!')
            });
    },[])

    const getZadatak2= useCallback(() => {
    TestAxios.get('/zadatak2')
    .then(res => {
        // handle success
        console.log(res)
        setZadatak2(res.data)
    })
    .catch(error => {
        // handle error
        console.log(error)
        alert('Error occured please try again!')
    });
    },[])

    const getZadatak3 = useCallback(() => {
        TestAxios.get('/zadatak3')
        .then(res => {
            // handle success
            console.log(res)
            setZadatak3(res.data)
        })
        .catch(error => {
            // handle error
            console.log(error)
            alert('Error occured please try again!')
        });
        },[])

    const create = () => {
        let zadatakDTO = {
            naziv: noviZadatak.naziv,
            cena: noviZadatak.cena,
            stanje: noviZadatak.stanje,
            zadatak2Id: noviZadatak.zadatak2Id
        }
        TestAxios.post("/zadatak1", zadatakDTO)
        .then(() => {
            window.location.reload();
        })
        .catch(error => {
            console.log(error)
            alert('Error while creating projection')
        })
    }


    const deleteZadatak = (zadatakId) => {
        TestAxios.delete('/zadatak1/' + zadatakId)
            .then(res => {
                // handle success
                console.log(res)
                window.location.reload();
            })
            .catch(error => {
                // handle error
                console.log(error)
                alert('Error occured please try again!')
            });
    }

    const onChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        noviZadatak[name] = value;
        setNoviZadatak(noviZadatak);
        console.log(noviZadatak);
      };

      const onChangeSearch = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        search[name] = value;
        setSearch(search);
        console.log(search);
      };

    const renderZad = () => {
        return zadatak1.map((zadatak) => {
            {console.log("zadatak: " + zadatak.zadatak2.naziv)}
            return <Zadatak1Row key={zadatak.id} zadatak={zadatak} deleteZadatakCallback={deleteZadatak}></Zadatak1Row>
        })
    }

    const renderZadatak2 = () => {
        return zadatak2.map((zad)=><option key={zad.id} value={zad.id}>{zad.naziv}</option>)
    }

    const renderZadatak3 = () => {
        return zadatak3.map((zad)=><option key={zad.id} value={zad.id}>{zad.naziv}</option>)
    }

    const renderSearchForm = () => {
        return (
            <>
            <Form style={{ width: "99%" }}>
                <Row><Col>
                    <Form.Group>
                        <Form.Label>Min Cena</Form.Label>
                        <Form.Control
                            name="minCena"
                            as="input"
                            type="number"
                            onChange={(e) => onChangeSearch(e)}></Form.Control>
                        <Form.Label>Max Cena</Form.Label>
                        <Form.Control
                            name="maxCena"
                            as="input"
                            type="number"
                            onChange={(e) => onChangeSearch(e)}></Form.Control>
                    </Form.Group>
                </Col></Row>

                <Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Kategorija</Form.Label>
                            <Form.Control as="select" 
                                name="zadatak2Id" 
                                onChange={(e) => onChangeSearch(e)}>
                                <option></option>
                                {renderZadatak2()}
                            </Form.Control><br />
                        </Form.Group>
                    </Col>
                </Row>
                
            </Form>
            <Row><Col>
                <Button className="mt-3" onClick={() => getZadatak1(0)}>Search</Button>
            </Col></Row>
            </>
        );
    }



    return (
        
        <Col>
            <Row><h1>Proizvod</h1></Row>

            {window.localStorage['svoja rola']=='ADMIN'?
            <Row>
            <Col xs="12" sm="10" md="8">
            <Form>
            <Form.Label htmlFor="naziv">Naziv</Form.Label>
            <Form.Control
              id="naziv"
              name="naziv"
              type="text"
              onChange={(e) => onChange(e)}
            />
            
            <Form.Label htmlFor="cena">Cena</Form.Label>
            <Form.Control
              id="cena"
              name="cena"
              type="number"
              onChange={(e) => onChange(e)}
            />
            <Form.Label htmlFor="stanje">Stanje</Form.Label>
            <Form.Control
              id="stanje"
              name="stanje"
              type="number"
              onChange={(e) => onChange(e)}
            />
            <Form.Label htmlFor="zadatak2Id">Kategorija</Form.Label>
            <Form.Control as="select" name="zadatak2Id" onChange={(e) => onChange(e)}>
                <option></option>
                {renderZadatak2()}
            </Form.Control><br />
            <Button style={{ marginTop: "25px" }} onClick={create}>
              Add
            </Button>
            </Form>
            </Col>
            </Row>: null}
            <br/>
            <br/> 

            <div>
                <label>
                <input type="checkbox" onChange={()=>setShowSearch(!showSearch)}/>
                    Show Search
                </label>
            </div>
            <Row hidden={!showSearch} >
                {renderSearchForm()}
            </Row>
            <br/>
            <br/>

            <Button disabled={pageNo===0} onClick={()=>getZadatak1(pageNo-1)} className="mr-3">Prev</Button>
            <Button disabled={pageNo==totalPages-1} onClick={()=>getZadatak1(pageNo+1)}>Next</Button>
            <Row><Col>
            <Table style={{marginTop: 5}}>
            <thead>
            <tr>
            <th>Naziv</th>
            <th>Cena (min)</th>
            <th>Stanje</th>
            <th>Kategorija</th>
            <th></th>
            </tr>
            </thead>
            <tbody>
                {renderZad()}
            </tbody>
            </Table>
            </Col></Row>
        </Col>
    );
}

export default Zadatak1